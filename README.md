# Webonary

This module provides integration with the **[SIL FieldWorks](https://software.sil.org/fieldworks/)**, a software tool that manages linguistic and cultural data. The Webonary module allows importing
data generated by the **SIL FieldWorks** software, allowing editors to have a single source for their linguistic data and easily share it with the website.

The Webonary module creates a new content entity called **Webonary Entity**, keeping all imported data away from the nodes. It also creates two taxonomies for classifying the content: **Webonary
Grammar Category** and **Webonary Language-Alphabet**. These taxonomies will be automatically generated when importing the data, but you can also customize those classifications within your website.

You can import both the **vernacular** (i.e., Osage to English) and **reversal** (i.e., English to Osage) dictionaries into the Drupal site. In addition to the individual page for each entity, each
dictionary gets its dedicated pages, which are easily accessible via customized URL paths like _domain.com/webonary/osage_ and _domain.com/webonary/english_.

Besides the listing pages, this module also provides a **search page** to look into both dictionaries. Users can explore the vernacular and reversal dictionaries effortlessly, aided by a customizable
virtual keyboard. This ensures accessibility, even for languages with unique characters.

---

## Audience:

The Webonary module leverages the linguistic and cultural data from the SIL FieldWorks software as a standardized import format to allow other languages to similarly integrate a web dictionary on
their Drupal website.

---

This module was developed with the support of **[The Osage Nation](https://www.osageculture.com/)**. You can see it in action on their website:

- https://www.osageculture.com/webonary/search
- https://www.osageculture.com/webonary/osage
- https://www.osageculture.com/webonary/english
