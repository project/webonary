<?php

/**
 * @file
 * Install, update and uninstall functions for the Webonary module.
 */

use Drupal\Core\Entity\EntityStorageException;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Implements hook_install().
 */
function webonary_install(): void {

  // Create the Vernacular and Reversal terms.
  $languages = [
    [
      'type' => 'vernacular',
      'name' => 'Vernacular',
      'code' => 'ver',
      'description' => 'The Webonary vernacular language.',
    ],
    [
      'type' => 'reversal',
      'name' => 'Reversal',
      'code' => 'rev',
      'description' => 'The Webonary reversal language.',
    ],
  ];

  foreach ($languages as $key => $value) {
    try {
      $term = Term::create([
        'parent' => [],
        'name' => $value['name'],
        'field_webonary_lang_code' => $value['code'],
        'weight' => $key,
        'vid' => 'webonary_language_alphabet',
        'description' => $value['description'],
      ]);

      if ($term->save()) {
        Drupal::configFactory()
          ->getEditable('webonary.settings')
          ->set($value['type'] . '_lang', $term->id())
          ->save();
      }
    }
    catch (EntityStorageException $e) {
      Drupal::messenger()->addWarning($e->getMessage());
    }
  }
}

/**
 * Implements hook_uninstall().
 */
function webonary_uninstall(): void {

  // Delete the vocabularies.
  $vocabularies = [
    'webonary_grammar_category',
    'webonary_language_alphabet',
  ];
  foreach ($vocabularies as $vid) {
    $vocabulary = Vocabulary::load($vid);
    if ($vocabulary) {
      try {
        $vocabulary->delete();
      }
      catch (EntityStorageException $e) {
        Drupal::messenger()->addError($e->getMessage());
      }
    }
  }

  // Delete the Webonary folder.
  if (is_dir('public://webonary')) {
    Drupal::service('file_system')
      ->deleteRecursive('public://webonary');
  }
}
