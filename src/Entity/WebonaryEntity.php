<?php

namespace Drupal\webonary\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\webonary\WebonaryEntityInterface;

/**
 * Defines the webonary entity class.
 *
 * @ContentEntityType(
 *   id = "webonary_entity",
 *   label = @Translation("Webonary"),
 *   label_collection = @Translation("Webonary Entities"),
 *   bundle_label = @Translation("Webonary Entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\webonary\WebonaryEntityAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\webonary\Form\WebonaryEntityForm",
 *       "edit" = "Drupal\webonary\Form\WebonaryEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "webonary_entity",
 *   admin_permission = "administer webonary entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "bundle",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/webonary/add/{webonary_entity_type}",
 *     "add-page" = "/admin/content/webonary/add",
 *     "canonical" = "/webonary/entry/{webonary_entity}",
 *     "edit-form" = "/admin/content/webonary/{webonary_entity}/edit",
 *     "delete-form" = "/admin/content/webonary/{webonary_entity}/delete"
 *   },
 *   bundle_entity_type = "webonary_entity_type",
 *   field_ui_base_route = "entity.webonary_entity_type.edit_form"
 * )
 */
class WebonaryEntity extends ContentEntityBase implements WebonaryEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getTitle(): string {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle(string $title): WebonaryEntityInterface {
    $this->set('title', $title);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus(bool $status): WebonaryEntityInterface {
    $this->set('status', $status);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $timestamp): WebonaryEntityInterface {
    $this->set('created', $timestamp);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['field_flexid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('FLExId'))
      ->setDescription(t('The ID related to the FieldWorks Language Explorer (FLEx) software.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Word'))
      ->setDescription(t('The word of the webonary entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 1,
        'settings' => [
          'link_to_entity' => FALSE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_definition'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Definition'))
      ->setDescription(t('The word definition.'))
      ->setRequired(TRUE)
      ->setDefaultValue([
        0 => [
          'value' => ' ',
          'format' => 'webonary_html',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 3,
        'settings' => [
          'rows' => 2,
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'text_default',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_definition_plain'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Definition Plain'))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_alphabet_group'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Alphabet Group'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => ['webonary_language_alphabet'],
        'sort' => [
          'field' => 'name',
          'direction' => 'asc',
        ],
        'auto_create' => FALSE,
        'auto_create_bundle' => '',
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_grammar_category'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Grammar Category'))
      ->setRequired(FALSE)
      ->setCardinality(-1)
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => ['webonary_grammar_category'],
        'sort' => [
          'field' => 'name',
          'direction' => 'asc',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_label',
        'weight' => 3,
        'settings' => [
          'link' => FALSE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_variants'] = BaseFieldDefinition::create('field_webonary_variant')
      ->setLabel(t('Variants'))
      ->setDescription(t('The variant IDs related to the FieldWorks Language Explorer (FLEx) software.'))
      ->setRequired(FALSE)
      ->setCardinality(-1)
      ->setDisplayOptions('form', [
        'type' => 'field_webonary_variant_widget',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('This field is used for sorting the entries on the page.'))
      ->setRequired(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'integer',
        'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Published'))
      ->setDescription(t('A boolean indicating whether the webonary entity is enabled.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Published')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the webonary entity was created.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the webonary entity was last edited.'))
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
