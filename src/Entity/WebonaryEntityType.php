<?php

namespace Drupal\webonary\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Webonary Entity type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "webonary_entity_type",
 *   label = @Translation("Webonary Entity type"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\webonary\Form\WebonaryEntityTypeForm",
 *       "edit" = "Drupal\webonary\Form\WebonaryEntityTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\webonary\WebonaryEntityTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer webonary entity types",
 *   bundle_of = "webonary_entity",
 *   config_prefix = "webonary_entity_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/webonary_entity_types/add",
 *     "edit-form" = "/admin/structure/webonary_entity_types/manage/{webonary_entity_type}",
 *     "delete-form" = "/admin/structure/webonary_entity_types/manage/{webonary_entity_type}/delete",
 *     "collection" = "/admin/structure/webonary_entity_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class WebonaryEntityType extends ConfigEntityBundleBase {

  /**
   * The machine name of this webonary entity type.
   *
   * @var string
   */
  protected string $id;

  /**
   * The human-readable name of the webonary entity type.
   *
   * @var string
   */
  protected string $label;

}
