<?php

namespace Drupal\webonary\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\webonary\Services\WebonaryService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for Webonary entity page.
 */
class PageController extends ControllerBase {

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The webonary service.
   *
   * @var \Drupal\webonary\Services\WebonaryService
   */
  protected WebonaryService $webonaryService;

  /**
   * Creates a new page controller.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity manager service.
   * @param \Drupal\webonary\Services\WebonaryService $webonary_service
   *   The webonary service.
   */
  public function __construct(EntityTypeManager $entity_type_manager, WebonaryService $webonary_service) {
    $this->entityTypeManager = $entity_type_manager;
    $this->webonaryService = $webonary_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): PageController {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('webonary'),
    );
  }

  /**
   * Builds the Webonary entity page.
   */
  public function buildPage(string $webonary_entity) {

    $storage = $this->entityTypeManager->getStorage('webonary_entity');

    // Validate whether the received id.
    if (strlen($webonary_entity) < 20) {
      // $variant = NULL;
      $entity = $storage->load($webonary_entity);
      if (isset($entity)) {

        // Redirects the user in case they opened the page using the entity id.
        $flexid = $entity->field_flexid->value ?? '';
        if (strlen($flexid) >= 20) {

          return $this->redirect('entity.webonary_entity.canonical', [
            'webonary_entity' => $flexid,
          ]);
        }
      }
    }
    else {

      // Extract the entity from a FLExId.
      $data = $this->webonaryService->getWebonaryEntity($webonary_entity);
      $entity = $data['entity'];
      // $variant = $data['variant'];
      if (isset($data['variant'])) {

        // If variant, redirects to the main entity.
        return $this->redirect('entity.webonary_entity.canonical', [
          'webonary_entity' => $entity->field_flexid->value ?? '',
        ]);
      }
    }

    if (!isset($entity)) {
      throw new NotFoundHttpException();
    }

    // Renders the entity.
    $builder = $this->entityTypeManager->getViewBuilder('webonary_entity');
    $render = $builder->view($entity);
    // $render['#variant'] = $variant ?? '';
    return $render;
  }

}
