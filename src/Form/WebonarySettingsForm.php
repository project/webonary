<?php

namespace Drupal\webonary\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Webonary settings for this site.
 */
class WebonarySettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'webonary_webonary_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['webonary.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $config = $this->config('webonary.settings');
    $lang_terms = $this->entityTypeManager
      ->getStorage('taxonomy_term')
      ->loadTree('webonary_language_alphabet', 0, 1);

    // Validate the number of terms.
    if (empty($lang_terms)) {
      $form['terms_needed'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $this->t('
            You must have a dictionary language for importing.
            <a href=":href">Manage vocabulary</a>
          ',
          [
            ':href' => '/admin/structure/taxonomy/manage/webonary_language_alphabet/overview',
          ]
        ),
      ];

      return $form;
    }

    // Set the options.
    $options = ['' => $this->t('- Select -')];
    foreach ($lang_terms as $lang_term) {
      $options[$lang_term->tid] = $lang_term->name;
    }

    $form['language'] = [
      '#type' => 'details',
      '#title' => $this->t('Language'),
      '#description' => $this->t('Select each type of language based on the dictionary terms. <a href=":href">Manage vocabulary</a>',
        [
          ':href' => '/admin/structure/taxonomy/manage/webonary_language_alphabet/overview',
        ]
      ),
      '#open' => TRUE,

      'vernacular_lang' => [
        '#type' => 'select',
        '#options' => $options,
        '#title' => $this->t('Vernacular'),
        '#default_value' => $config->get('vernacular_lang') ?? '',
        '#required' => TRUE,
      ],

      'reversal_lang' => [
        '#type' => 'select',
        '#options' => $options,
        '#title' => $this->t('Reversal'),
        '#default_value' => $config->get('reversal_lang') ?? '',
        '#required' => FALSE,
      ],
    ];

    $form['views'] = [
      '#type' => 'details',
      '#title' => $this->t('Webonary Path'),
      '#description' => $this->t('Define the final path for each dictionary on the <strong>/webonary/%dictionary</strong> pages.'),
      '#open' => TRUE,

      'vernacular_path' => [
        '#type' => 'textfield',
        '#title' => $this->t('Vernacular'),
        '#default_value' => $config->get('vernacular_path') ?? 'vernacular',
        '#required' => TRUE,
        '#description' => $this->t('Use a valid URL string. The system might remove invalid characters from this field.'),
      ],

      'reversal_path' => [
        '#type' => 'textfield',
        '#title' => $this->t('Reversal'),
        '#default_value' => $config->get('reversal_path') ?? 'reversal',
        '#required' => TRUE,
        '#description' => $this->t('Use a valid URL string. The system might remove invalid characters from this field.'),
      ],
    ];

    $form['#attached']['library'][] = 'webonary/import.settings';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $vernacular_lang = $form_state->getValue('vernacular_lang');
    $reversal_lang = $form_state->getValue('reversal_lang');
    if ($vernacular_lang === $reversal_lang) {
      $form_state->setErrorByName('reversal_lang', $this->t('The reversal language must be different from the vernacular one.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('webonary.settings')
      ->set('vernacular_lang', $form_state->getValue('vernacular_lang'))
      ->set('reversal_lang', $form_state->getValue('reversal_lang'))
      ->set('vernacular_path', $this->clearPath($form_state->getValue('vernacular_path'), 'vernacular'))
      ->set('reversal_path', $this->clearPath($form_state->getValue('reversal_path'), 'reversal'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Clears the URL path.
   *
   * @param string $path
   *   The entered path.
   * @param string $default
   *   The default value for the path.
   *
   * @return string
   *   The clean path.
   */
  protected function clearPath(string $path, string $default): string {
    $path = preg_replace('/[^A-Za-z0-9\-]/', ' ', $path);

    $valid = '';
    foreach (explode(' ', $path) as $item) {
      if (!empty($item) && strtolower($item) !== 'webonary') {
        $valid = $item;
        break;
      }
    }

    return empty($valid) ? $default : $valid;
  }

}
