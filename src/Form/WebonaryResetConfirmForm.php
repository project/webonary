<?php

namespace Drupal\webonary\Form;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\webonary\Classes\EImportDictionary;
use Drupal\webonary\Services\WebonaryService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a confirmation form before clearing out the Webonary.
 */
class WebonaryResetConfirmForm extends ConfirmFormBase {

  /**
   * The Webonary Service.
   *
   * @var \Drupal\webonary\Services\WebonaryService
   */
  protected WebonaryService $webonaryService;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new reset form.
   *
   * @param \Drupal\webonary\Services\WebonaryService $webonary_service
   *   The Webonary Service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager service.
   */
  public function __construct(WebonaryService $webonary_service, EntityTypeManagerInterface $entity_type_manager) {
    $this->webonaryService = $webonary_service;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): WebonaryResetConfirmForm {
    return new static(
      $container->get('webonary'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'webonary_reset_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Do you want to reset the Webonary data?');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): TranslatableMarkup {
    return $this->t('This action will delete <strong>all</strong> data related to the Webonary, including content and taxonomy entries.<br><strong>This action cannot be undone.</strong><br>Only proceed if you are sure of what you are doing.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return new Url('webonary.admin_config_system');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      $entityHandler = $this->entityTypeManager->getStorage('webonary_entity');
      $taxonomyHandler = $this->entityTypeManager->getStorage('taxonomy_term');

      // Clean up the Webonary entities.
      $webonaryEntities = $entityHandler->loadMultiple();
      $entityHandler->delete($webonaryEntities);

      // Clean up the Webonary Grammar Category taxonomies.
      $tids = $taxonomyHandler->getQuery()
        ->condition('vid', 'webonary_grammar_category')
        ->accessCheck(TRUE)
        ->execute();
      $webonaryGrammarCategories = $taxonomyHandler->loadMultiple($tids);
      $taxonomyHandler->delete($webonaryGrammarCategories);

      // Clean up the Webonary Language-Alphabet taxonomies.
      $tids = $taxonomyHandler->getQuery()
        ->condition('vid', 'webonary_language_alphabet')
        ->condition('parent', 0, '!=')
        ->accessCheck(TRUE)
        ->execute();
      $webonaryLanguageAlphabet = $taxonomyHandler->loadMultiple($tids);
      $taxonomyHandler->delete($webonaryLanguageAlphabet);

      // Reset the state.
      $this->webonaryService->deleteStyleFile(EImportDictionary::VERNACULAR);
      $this->webonaryService->deleteStyleFile(EImportDictionary::REVERSAL);
      $this->webonaryService->deleteMediaFolder(EImportDictionary::VERNACULAR);
      $this->webonaryService->deleteMediaFolder(EImportDictionary::REVERSAL);

      $this->messenger()->addStatus($this->t('Webonary content reset!'));
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException | EntityStorageException $e) {
      $this->messenger()->addError($this->t('Failed to reset the Webonary content!'));
    }

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
