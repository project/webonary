<?php

namespace Drupal\webonary\Form;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\webonary\Classes\EImportDictionary;
use Drupal\webonary\Classes\EImportType;
use Drupal\webonary\Classes\ImportSchema;
use Drupal\webonary\Services\ImportService;
use Drupal\webonary\Services\WebonaryService;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Webonary form.
 */
class WebonaryImportForm extends FormBase {

  /**
   * The Webonary import service.
   *
   * @var \Drupal\webonary\Services\WebonaryService
   */
  protected WebonaryService $webonaryService;

  /**
   * The Webonary import service.
   *
   * @var \Drupal\webonary\Services\ImportService
   */
  protected ImportService $importService;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The Webonary logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Creates a new import service.
   *
   * @param \Drupal\webonary\Services\WebonaryService $webonary_service
   *   The Webonary service.
   * @param \Drupal\webonary\Services\ImportService $import_service
   *   The Webonary import service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The Webonary logger channel.
   */
  public function __construct(WebonaryService $webonary_service, ImportService $import_service, EntityTypeManagerInterface $entity_type_manager, LoggerInterface $logger) {
    $this->webonaryService = $webonary_service;
    $this->importService = $import_service;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): WebonaryImportForm {
    return new static(
      $container->get('webonary'),
      $container->get('webonary.import'),
      $container->get('entity_type.manager'),
      $container->get('logger.channel.webonary')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'webonary_import';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    try {
      $uploadLocation = 'temporary://webonary/' . date("YmdHis");
      $dictionaries = $this->webonaryService->getDictionaries();

      if (!isset($dictionaries[EImportDictionary::VERNACULAR])) {
        $form['settings_needed'] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $this->t('
              You must set your vernacular dictionary for importing.
              <a href=":href">Settings</a>
            ',
            [
              ':href' => '/admin/config/webonary/settings',
            ]
          ),
        ];
      }
      else {

        // Get the dictionary types.
        $dictionaryOptions = [
          EImportDictionary::VERNACULAR => $this->t('Vernacular') . '<small>' . $dictionaries[EImportDictionary::VERNACULAR]['name'] . '</small>',
        ];
        if (isset($dictionaries[EImportDictionary::REVERSAL])) {
          $dictionaryOptions[EImportDictionary::REVERSAL] = $this->t('Reversal') . '<small>' . $dictionaries[EImportDictionary::REVERSAL]['name'] . '</small>';
        }

        $form['dictionary_type'] = [
          '#type' => 'details',
          '#title' => $this->t('Type of Dictionary'),
          '#open' => TRUE,

          'import_dictionary' => [
            '#type' => 'radios',
            '#title' => $this->t('Select the type of dictionary to import:'),
            '#required' => TRUE,
            '#options' => $dictionaryOptions,
            '#default_value' => count($dictionaryOptions) === 1 ? EImportDictionary::VERNACULAR : NULL,
          ],

          'import_type' => [
            '#type' => 'radios',
            '#title' => $this->t('Select the type of import:'),
            '#required' => TRUE,
            '#options' => [
              EImportType::FULL => $this->t('Full <small>This option will <strong>delete</strong> all existing entries and <strong>create</strong> new entries based on the import file.</small>'),
              EImportType::PARTIAL => $this->t('Partial <small>This option will <strong>keep</strong> all existing entries and either <strong>create</strong> new entries or <strong>update</strong> existing entries based on the import file.</small>'),
            ],
          ],
        ];

        $form['definition_file'] = [
          '#type' => 'details',
          '#title' => $this->t('Definition File'),
          '#open' => TRUE,
          '#states' => [
            'visible' => [
              ':input[name="import_dictionary"]' => ['checked' => TRUE],
              ':input[name="import_type"]' => ['checked' => TRUE],
            ],
          ],

          'instruction' => [
            '#type' => 'container',
            'full_vernacular' => $this->renderInstruction(EImportDictionary::VERNACULAR, EImportType::FULL),
            'partial_vernacular' => $this->renderInstruction(EImportDictionary::VERNACULAR, EImportType::PARTIAL),
            'full_reversal' => $this->renderInstruction(EImportDictionary::REVERSAL, EImportType::FULL),
            'partial_reversal' => $this->renderInstruction(EImportDictionary::REVERSAL, EImportType::PARTIAL),
          ],

          'data_file_upload' => [
            '#type' => 'managed_file',
            '#title' => $this->t('Select the exported XHTML file:'),
            '#required' => TRUE,
            '#description' => $this->t('Allowed types: @extensions.', ['@extensions' => 'xhtml, xml']),
            '#upload_location' => $uploadLocation,
            '#upload_validators' => [
              'file_validate_extensions' => ['xhtml xml'],
            ],
          ],
        ];

        $form['styles'] = [
          '#type' => 'details',
          '#title' => $this->t('Custom Styles'),
          '#open' => TRUE,
          '#states' => [
            'visible' => [
              ':input[name="import_dictionary"]' => ['checked' => TRUE],
              ':input[name="import_type"]' => ['checked' => TRUE],
            ],
          ],

          'style_options' => [
            '#type' => 'radios',
            '#default_value' => 'keep',
            '#options' => [
              'keep' => $this->t('Keep the previous CSS format'),
              'replace' => $this->t('Replace the CSS format'),
            ],
          ],

          'style_replace' => [
            '#type' => 'container',
            '#states' => [
              'visible' => [
                ':input[name="style_options"]' => ['value' => 'replace'],
              ],
            ],

            'instruction' => [
              '#type' => 'html_tag',
              '#tag' => 'div',
              '#value' => $this->t('
                  The CSS file is exported to the same folder as the dictionary file.<br>
                  This folder will contain a <strong>.css</strong> file that can be uploaded below.<br>
                  <a href=":href" target="_blank">Learn more</a>
                ',
                [
                  ':href' => 'https://software.sil.org/fieldworks/wp-content/uploads/sites/38/2018/03/Export-options-in-Flex.pdf',
                ]
              ),
              '#attributes' => [
                'class' => 'tutorial-text',
              ],
            ],

            'style_file_upload' => [
              '#type' => 'managed_file',
              '#title' => $this->t('Select the exported CSS file:'),
              '#description' => $this->t('Allowed types: @extensions.', ['@extensions' => 'css']),
              '#upload_location' => $uploadLocation,
              '#upload_validators' => [
                'file_validate_extensions' => ['css'],
              ],
            ],
          ],
        ];

        $form['media'] = [
          '#type' => 'details',
          '#title' => $this->t('Media Files'),
          '#open' => TRUE,
          '#states' => [
            'visible' => [
              ':input[name="import_dictionary"]' => ['value' => EImportDictionary::VERNACULAR],
              ':input[name="import_type"]' => ['checked' => TRUE],
            ],
          ],

          'media_options' => [
            '#type' => 'radios',
            '#default_value' => 'keep',
            '#options' => [
              'keep' => $this->t('Keep the previous media files'),
              'replace' => $this->t('Replace the media files'),
            ],
          ],

          'media_replace' => [
            '#type' => 'container',
            '#states' => [
              'visible' => [
                ':input[name="media_options"]' => ['value' => 'replace'],
              ],
            ],

            'instruction' => [
              '#type' => 'html_tag',
              '#tag' => 'div',
              '#value' => $this->t('
                  The media files are exported to the same folder as the dictionary file.
                  <ol>
                    <li>The audio and image files are exported to subfolders called <strong>AudioVisual</strong> and <strong>pictures</strong>.</li>
                    <li>Select those two folders and compress them into a single <strong>zip</strong> file.</li>
                  </ol>
                  The <strong>zip</strong> file can be uploaded below.
                  <a href=":href" target="_blank">Learn more</a>
                ',
                [
                  ':href' => 'https://www.tezu.ernet.in/wmcfel/pdf/Cog/lexico/04.pdf',
                ]
              ),
              '#attributes' => [
                'class' => 'tutorial-text',
              ],
            ],

            'media_file_upload' => [
              '#type' => 'managed_file',
              '#title' => $this->t('Select the ZIP file:'),
              '#description' => $this->t('Allowed types: @extensions.', ['@extensions' => 'zip']),
              '#upload_location' => $uploadLocation,
              '#upload_validators' => [
                'file_validate_extensions' => ['zip'],
              ],
            ],
          ],
        ];

        $form['actions'] = [
          '#type' => 'actions',
          'submit' => [
            '#type' => 'submit',
            '#value' => $this->t('Import'),
          ],
        ];

        $form['#attached']['library'][] = 'webonary/import.form';
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      $this->logger->error($e->getMessage());
    }

    return $form;
  }

  /**
   * Render instructions for the definition file.
   *
   * @param string $import_dictionary
   *   The type of dictionary to import.
   * @param string $import_type
   *   The type of import.
   *
   * @return array
   *   Render object.
   */
  protected function renderInstruction(string $import_dictionary, string $import_type): array {

    $instruction = '';
    switch ($import_type . '_' . $import_dictionary) {
      case 'full_vernacular':
        $instruction = $this->t('
            To export a full vernacular dictionary from the FieldWorks software:
            <ol>
              <li>On the bottom left, ensure "Lexicon" is selected.</li>
              <li>Ensure the column header shown in "Entries" are all set to "Show All"</li>
              <li>On the top left, click on the menu item "File".</li>
              <li>Click "Export...".</li>
              <li>Click "Configured Dictionary Web page (XHTML)".</li>
              <li>Click "Export..." button and choose the folder where you want to save it.</li>
            </ol>
            The dictionary file will be exported to the folder with a <strong>.xhtml</strong> extension.<br>
            <a href=":href" target="_blank">Learn more</a>
          ',
          [
            ':href' => 'https://software.sil.org/fieldworks/resources/tutorial/lexicon/import-export/',
          ]
        );
        break;

      case 'partial_vernacular':
        $instruction = $this->t('
            To export a partial vernacular dictionary from FieldWorks software:
            <ol>
              <li>On the bottom left, ensure "Lexicon" is selected. If not, then click on it.</li>
              <li>Ensure the column head shown in "Entries" is set to the desired filter.</li>
              <li>On the top left, click on the menu item "File".</li>
              <li>Click on "Export...".</li>
              <li>Click on "Configured Dictionary Web page (XHTML)".</li>
              <li>Click on the "Export..." button and choose the folder where you want to save it.</li>
            </ol>
            The dictionary file will be exported to the folder with a ".xhtml" extension.<br>
            <a href=":href" target="_blank">Learn more</a>
          ',
          [
            ':href' => 'https://software.sil.org/fieldworks/resources/tutorial/lexicon/import-export/',
          ]
        );
        break;

      case 'full_reversal':
        $instruction = $this->t('
            To export a full reversal dictionary from FieldWorks software:
            <ol>
              <li>On the bottom left, ensure "Lexicon" is selected. If not, then click on it.</li>
              <li>Ensure the column head shown in "Entries" are all set to "Show All"</li>
              <li>On the top left, click on the menu item "File".</li>
              <li>Click on "Export...".</li>
              <li>Click on "Reversal Index Web page (XHTML)".</li>
              <li>Click on the "Export..." button and choose the folder where you want to save it.</li>
            </ol>
            The dictionary file will be exported to the folder with a ".xhtml" extension.<br>
            <a href=":href" target="_blank">Learn more</a>
          ',
          [
            ':href' => 'https://software.sil.org/fieldworks/resources/tutorial/lexicon/import-export/',
          ]
        );
        break;

      case 'partial_reversal':
        $instruction = $this->t('
            To export a partial reversal dictionary from FieldWorks software:
            <ol>
              <li>On the bottom left, ensure "Lexicon" is selected. If not, then click on it.</li>
              <li>Ensure the column head shown in "Entries" are all set to the desired filter.</li>
              <li>On the top left, click on the menu item "File".</li>
              <li>Click on "Export...".</li>
              <li>Click on "Reversal Index Web page (XHTML)".</li>
              <li>Click on the "Export..." button and choose the folder where you want to save it.</li>
            </ol>
            The dictionary file will be exported to the folder with a ".xhtml" extension.<br>
            <a href=":href" target="_blank">Learn more</a>
          ',
          [
            ':href' => 'https://software.sil.org/fieldworks/resources/tutorial/lexicon/import-export/',
          ]
        );
        break;
    }

    return [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="import_dictionary"]' => ['value' => $import_dictionary],
          ':input[name="import_type"]' => ['value' => $import_type],
        ],
      ],

      'instruction' => [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $instruction,
        '#attributes' => [
          'class' => 'tutorial-text',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (!in_array($form_state->getValue('import_dictionary'), [
      EImportDictionary::VERNACULAR,
      EImportDictionary::REVERSAL,
    ])) {
      $form_state->setErrorByName('import_dictionary', $this->t('The selected type of dictionary is not valid.'));
    }

    if (!in_array($form_state->getValue('import_type'), [
      EImportType::FULL,
      EImportType::PARTIAL,
    ])) {
      $form_state->setErrorByName('import_type', $this->t('The selected type of import is not valid.'));
    }

    if (empty($form_state->getValue('data_file_upload'))) {
      $form_state->setErrorByName('data_file_upload', $this->t('You must select the exported XHTML file.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    try {
      $fileStorage = $this->entityTypeManager->getStorage('file');

      // Gets all values.
      $dictionary = $form_state->getValue('import_dictionary');
      $type = $form_state->getValue('import_type');

      $dataFile = '';
      if (!empty($form_state->getValue('data_file_upload'))) {
        $dataFile = reset($form_state->getValue('data_file_upload'));
        $dataFile = $fileStorage->load($dataFile);
        $dataFile = $dataFile->getFileUri();
      }

      $styleFile = '';
      if (!empty($form_state->getValue('style_file_upload'))) {
        $styleFile = reset($form_state->getValue('style_file_upload'));
        $styleFile = $fileStorage->load($styleFile);
        $styleFile = $styleFile->getFileUri();
      }

      $mediaFile = '';
      if (!empty($form_state->getValue('media_file_upload'))) {
        $mediaFile = reset($form_state->getValue('media_file_upload'));
        $mediaFile = $fileStorage->load($mediaFile);
        $mediaFile = $mediaFile->getFileUri();
      }

      // Execute the import.
      $importSchema = $this->importService->execute(
        new ImportSchema($dictionary, $type, $dataFile, $styleFile, $mediaFile)
      );

      // Validate the import result.
      if ($importSchema->imported) {
        $this->messenger()->addStatus($this->t('The dictionary was successfully imported.'));
        $this->logger->notice("Successfully executed a '%type' import for the '%dictionary' dictionary.", [
          '%type' => $type,
          '%dictionary' => $dictionary,
        ]);

        $form_state->setRedirect('view.webonary.webonary_content');
      }
      else {
        $errorMessage = $this->t('Failed to import the dictionary:') . '<ul>';
        foreach ($importSchema->errorMessages as $error) {
          $errorMessage .= '<li>' . $error . '</li>';
        }
        $errorMessage .= '</ul>';

        $this->messenger()->addError(Markup::create($errorMessage));

        $this->logger->error("Failed to execute a '%type' import for the '%dictionary' dictionary.", [
          '%type' => $type,
          '%dictionary' => $dictionary,
        ]);
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      $this->logger->error($e->getMessage());
    }
  }

}
