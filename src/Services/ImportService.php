<?php

namespace Drupal\webonary\Services;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Archiver\ArchiverManager;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\webonary\Classes\EImportDictionary;
use Drupal\webonary\Classes\EImportType;
use Drupal\webonary\Classes\ImportSchema;
use Psr\Log\LoggerInterface;
use Sabberworm\CSS\Parser;

/**
 * This service will execute all import actions.
 */
class ImportService {

  use StringTranslationTrait;

  /**
   * The Webonary Service.
   *
   * @var \Drupal\webonary\Services\WebonaryService
   */
  protected WebonaryService $webonaryService;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * The ImportSchema object containing the information to be imported.
   *
   * @var \Drupal\webonary\Classes\ImportSchema
   */
  protected ImportSchema $importSchema;

  /**
   * The list of available Webonary Alphabet taxonomy terms.
   *
   * @var array
   */
  protected array $alphabetTerms;

  /**
   * The list of available Webonary Grammar Category taxonomy terms.
   *
   * @var array
   */
  protected array $grammarTerms;

  /**
   * The loaded XHTML document.
   *
   * @var \DOMDocument
   */
  protected \DOMDocument $loadedDictionary;

  /**
   * A list of all current entities in the database.
   *
   * @var array
   */
  protected array $webonaryEntities;

  /**
   * Entity type manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Entity type manager interface.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected ConfigFactory $config;

  /**
   * Archiver manager.
   *
   * @var \Drupal\Core\Archiver\ArchiverManager
   */
  protected ArchiverManager $archiverManager;

  /**
   * Module extension list service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected ModuleExtensionList $moduleExtensionList;

  /**
   * The Webonary logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * The file system controller.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected FileSystem $fileSystem;

  /**
   * File Url Generator Service.
   *
   * @var \Drupal\Core\File\FileUrlGenerator
   */
  protected FileUrlGenerator $fileUrlGenerator;

  /**
   * The style folder destination.
   *
   * @var string
   */
  protected string $styleDestination;

  /**
   * The media folder destination.
   *
   * @var string
   */
  protected string $mediaDestination;

  /**
   * Constructs an ImportService object.
   *
   * @param \Drupal\webonary\Services\WebonaryService $webonary_service
   *   The Webonary Service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The service for managing entities.
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   The service for managing configurations.
   * @param \Drupal\Core\Archiver\ArchiverManager $archiver_manager
   *   The service for managing zip files.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   *   The service for managing zip files.
   * @param \Psr\Log\LoggerInterface $logger
   *   The Webonary logger channel.
   * @param \Drupal\Core\File\FileSystem $file_system
   *   The file system controller.
   * @param \Drupal\Core\File\FileUrlGenerator $file_url_generator
   *   File Url Generator Service.
   */
  public function __construct(WebonaryService $webonary_service, Connection $connection, TranslationInterface $string_translation, EntityTypeManagerInterface $entity_type_manager, ConfigFactory $config, ArchiverManager $archiver_manager, ModuleExtensionList $module_extension_list, LoggerInterface $logger, FileSystem $file_system, FileUrlGenerator $file_url_generator) {
    $this->webonaryService = $webonary_service;
    $this->connection = $connection;
    $this->stringTranslation = $string_translation;
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config;
    $this->archiverManager = $archiver_manager;
    $this->moduleExtensionList = $module_extension_list;
    $this->logger = $logger;
    $this->fileSystem = $file_system;
    $this->fileUrlGenerator = $file_url_generator;

    $this->styleDestination = '';
    $this->mediaDestination = '';
  }

  /**
   * Executes the import tasks.
   *
   * @param \Drupal\webonary\Classes\ImportSchema $importSchema
   *   An ImportSchema object containing the information to be imported.
   *
   * @return \Drupal\webonary\Classes\ImportSchema
   *   The updated ImportSchema object containing the information about the
   *   result of the import.
   */
  public function execute(ImportSchema $importSchema): ImportSchema {
    $this->importSchema = $importSchema;

    // Validate the data path.
    if ($this->importSchema->dataFilePath === '') {
      $this->importSchema->setError($this->t('The data file is required.'));

      return $this->importSchema;
    }

    // Validates the dictionary set to import.
    $language = $this->getImportLanguage();
    if ($language === '') {
      return $this->importSchema;
    }

    // Validates the type of import.
    $importType = $this->getImportType();
    if ($importType === '') {
      return $this->importSchema;
    }

    // Load the selected XHTML file.
    $hasLoadedDictionary = $this->loadDictionary();
    if (!$hasLoadedDictionary) {
      return $this->importSchema;
    }

    // Handles the style import.
    $styleFileSaved = $this->saveStyleFile();
    if (!$styleFileSaved) {
      return $this->importSchema;
    }

    // Handles the media import.
    $mediaFilesSaved = $this->saveMediaFiles();
    if (!$mediaFilesSaved) {
      return $this->importSchema;
    }

    // Imports all auxiliary data.
    $this->loadAlphabetTerms($language);
    $this->loadGrammarTerms();
    $this->loadWebonaryEntities();

    // Deletes the current entities for full import.
    if ($importType === EImportType::FULL) {
      $previousImportDeleted = $this->deletePreviousImport();
      if (!$previousImportDeleted) {
        return $this->importSchema;
      }
    }

    // Handles the data import.
    $this->importSchema->imported = $this->importSchema->dictionary === EImportDictionary::VERNACULAR
      ? $this->importVernacularDictionary($language)
      : $this->importReversalDictionary($language);

    // Delete CSS and media files that are no longer necessary.
    if ($this->importSchema->imported) {
      $this->cleanStyleImports();
      $this->cleanMediaImports();
    }

    return $this->importSchema;
  }

  /**
   * Validates the dictionary to import.
   *
   * @return string
   *   The tid of the language to import.
   */
  protected function getImportLanguage(): string {
    $language = '';

    try {
      $dictionaries = $this->webonaryService->getDictionaries();
      if (isset($dictionaries[$this->importSchema->dictionary])) {
        $language = $dictionaries[$this->importSchema->dictionary]['id'] ?? '';
      }

      if (empty($language)) {
        $this->importSchema->setError($this->t("The dictionary '%dictionary' is not valid.", [
          '%dictionary' => $this->importSchema->dictionary,
        ]));
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      $this->importSchema->setError($this->t("Failed to get the dictionary '%dictionary'.", [
        '%dictionary' => $this->importSchema->dictionary,
      ]));
      $this->logger->error($e->getMessage());
    }

    return $language;
  }

  /**
   * Validates the import type.
   *
   * @return string
   *   The type of import.
   */
  protected function getImportType(): string {
    $validImportTypes = [
      EImportType::FULL,
      EImportType::PARTIAL,
    ];

    if (!in_array($this->importSchema->type, $validImportTypes)) {
      $this->importSchema->setError($this->t("The import type '%type' is not valid.",
        [
          '%type' => $this->importSchema->type,
        ]
      ));

      return '';
    }

    return $this->importSchema->type;
  }

  /**
   * Loads dictionary from file.
   *
   * @return bool
   *   Returns true when successfully loaded and validated.
   */
  protected function loadDictionary(): bool {
    try {
      // Loads the selected XHTML file.
      $this->loadedDictionary = new \DOMDocument();
      libxml_use_internal_errors(TRUE);
      $htmlCode = file_get_contents($this->importSchema->dataFilePath);
      $this->loadedDictionary->loadHTML($htmlCode);

      // Gets the type of the loaded dictionary.
      $loadedDictionaryType = $this->getLoadedDictionaryType();
      if ($loadedDictionaryType !== $this->importSchema->dictionary) {
        $this->importSchema->setError($this->t('The uploaded file does not match the selected dictionary type.'));
        $this->logger->error('Unable to determine type of dictionary (vernacular or reversal)');

        return FALSE;
      }

      return TRUE;
    }
    catch (\Exception $e) {
      $this->importSchema->setError($this->t("Unable to load dictionary from file"));
      $this->logger->error('Failed to load the dictionary from file: ' . $e->getMessage());

      return FALSE;
    }
  }

  /**
   * Gets the type of the loaded dictionary.
   *
   * @return string
   *   Returns the type of dictionary.
   */
  protected function getLoadedDictionaryType(): string {
    $xPath = new \DOMXPath($this->loadedDictionary);

    $reversal = $xPath->query('//div[@class="reversalindexentry"]');
    if ($reversal->count() > 0) {
      return EImportDictionary::REVERSAL;
    }

    $vernacular = $xPath->query('//div[@class="entry"]');
    if ($vernacular->count() > 0) {
      return EImportDictionary::VERNACULAR;
    }

    return '';
  }

  /**
   * Saves the CSS file to its destination.
   *
   * @return bool
   *   Whether the file was successfully saved.
   */
  protected function saveStyleFile(): bool {
    // Validate whether the CSS file is being imported since it is optional.
    if ($this->importSchema->styleFilePath === '') {
      return TRUE;
    }

    // Copy the style file into the appropriated folder
    // to be injected on the page.
    try {
      if (!file_exists(ImportSchema::STYLE_FOLDER)) {
        $directory = ImportSchema::STYLE_FOLDER;
        $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
      }

      // Remove unnecessary selectors to avoid conflict.
      $parser = new Parser(file_get_contents($this->importSchema->styleFilePath));
      $cssDocument = $parser->parse();
      $cssDocument->removeDeclarationBlockBySelector('html', TRUE);
      $cssDocument->removeDeclarationBlockBySelector('body', TRUE);
      $cssDocument->removeDeclarationBlockBySelector('*', TRUE);
      $cssDocument->removeDeclarationBlockBySelector("*[dir='ltr'], *[dir='rtl']", TRUE);

      // Include a parent selector to make the markups more specific.
      foreach ($cssDocument->getAllDeclarationBlocks() as $block) {
        // Loop over all selector parts (the comma-separated strings in a
        // selector) and prepend the webonary class.
        foreach ($block->getSelectors() as $selector) {
          $selector->setSelector('.view-webonary ' . $selector->getSelector());
        }

        // Remove rules that could create conflict with the website styles.
        $block->removeRule('font-family');
        $block->removeRule('flex-line-height');
        $block->removeRule('line-height');
        $block->removeRule('-moz-column-count');
        $block->removeRule('-webkit-column-count');
        $block->removeRule('column-count');
        $block->removeRule('font-size');
      }
      $finalCss = $cssDocument->render();

      // Replace the line breaks with the correct character.
      $finalCss = str_replace('"\\A', '"\\0A ', $finalCss);

      // Copy the file.
      $this->styleDestination = ImportSchema::STYLE_FOLDER . '/styles.' . date("YmdHis") . '.css';
      $this->fileSystem->saveData($finalCss, $this->styleDestination);

      return TRUE;
    }
    catch (\Exception $e) {
      $this->importSchema->setError($this->t("Unable to load style file into webonary module"));
      $this->logger->error('Failed to save style file: ' . $e->getMessage());
    }

    return FALSE;
  }

  /**
   * Deletes the CSS files not being used.
   */
  protected function cleanStyleImports() {
    if (!empty($this->styleDestination)) {
      // Save the new location.
      $this->webonaryService->setStyleFile($this->importSchema->dictionary, $this->styleDestination);

      // Delete files in the CSS directory that are no longer in use.
      $inUse = $this->webonaryService->getStyleFiles();
      $scans = $this->fileSystem->scanDirectory(ImportSchema::STYLE_FOLDER, "/.+/");
      foreach ($scans as $file) {
        if (!in_array($file->uri, $inUse)) {
          $this->fileSystem->delete($file->uri);
        }
      }
    }
  }

  /**
   * Saves the media files to their destination.
   *
   * @return bool
   *   Whether the files were successfully saved.
   */
  protected function saveMediaFiles(): bool {
    // Validate whether the media files are being imported (it is optional).
    if ($this->importSchema->mediaFilePath === '') {
      return TRUE;
    }

    try {
      if (!file_exists(ImportSchema::MEDIA_FOLDER)) {
        $directory = ImportSchema::MEDIA_FOLDER;
        $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
      }

      // Unzip the compressed file and save all media to their destination.
      $this->mediaDestination = ImportSchema::MEDIA_FOLDER . '/media_' . date("YmdHis");
      $instance = $this->archiverManager->getInstance(['filepath' => $this->importSchema->mediaFilePath]);
      $instance->extract($this->mediaDestination);

      return TRUE;
    }
    catch (\Exception $e) {
      $this->importSchema->setError($this->t("Unable to extract media files into public webonary folder."));
      $this->logger->error('Unable to save media files: ' . $e->getMessage());
    }

    return FALSE;
  }

  /**
   * Deletes media files not being used.
   */
  protected function cleanMediaImports() {
    if (!empty($this->mediaDestination)) {
      // Save the new location.
      $this->webonaryService->setMediaFolder($this->importSchema->dictionary, $this->mediaDestination);

      // Delete files in the media directory that are no longer in use.
      $inUse = ['.', '..', 'failed'];
      foreach ($this->webonaryService->getMediaFolders() as $path) {
        $inUse[] = basename($path);
      }

      $scans = scandir($this->fileSystem->realpath(ImportSchema::MEDIA_FOLDER));
      foreach ($scans as $folder) {
        if (!in_array($folder, $inUse)) {
          $this->fileSystem->deleteRecursive(ImportSchema::MEDIA_FOLDER . '/' . $folder);
        }
      }
    }
  }

  /**
   * Deletes all Webonary entities related to the import.
   *
   * @return bool
   *   Whether the previous files were successfully deleted.
   */
  protected function deletePreviousImport(): bool {
    // Deletes all Webonary entities related to the received language.
    try {
      foreach ($this->webonaryEntities as $loadedEntity) {
        $loadedEntity->delete();
      }
      $this->webonaryEntities = [];

      return TRUE;
    }
    catch (\Exception $e) {
      $this->importSchema->setError($this->t('Unable to clear the previous import.'));
      $this->logger->error('Unable to clear the previous import: ' . $e->getMessage());
    }

    return FALSE;
  }

  /**
   * Loads all alphabet taxonomy terms.
   *
   * @param string $language
   *   The tid of the language to filter the entries to be loaded.
   */
  protected function loadAlphabetTerms(string $language) {
    $this->alphabetTerms = [];

    // Loads the Webonary Language-Alphabet taxonomy terms,
    // but only the second level related to the given language.
    try {
      $loadedEntities = $this->entityTypeManager
        ->getStorage('taxonomy_term')
        ->loadChildren($language);
      foreach ($loadedEntities as $loadedEntity) {
        $code = $loadedEntity->field_webonary_lang_code->value;
        $this->alphabetTerms[$code] = $loadedEntity->id();
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      $this->importSchema->setError($this->t('Failed to load the alphabet terms.'));
      $this->logger->error('Failed to load the alphabet terms: ' . $e->getMessage());
    }
  }

  /**
   * Loads all Webonary Grammar Category taxonomy terms.
   */
  protected function loadGrammarTerms() {
    $this->grammarTerms = [];

    // Loads the Grammar taxonomy terms.
    try {
      $loadedEntities = $this->entityTypeManager
        ->getStorage('taxonomy_term')
        ->loadByProperties(['vid' => 'webonary_grammar_category']);
      foreach ($loadedEntities as $loadedEntity) {
        $abbr = $loadedEntity->field_webonary_category_abbr->value;
        $this->grammarTerms[$abbr] = $loadedEntity->id();
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      $this->importSchema->setError($this->t('Failed to load the grammar terms.'));
      $this->logger->error('Failed to load the grammar terms: ' . $e->getMessage());
    }
  }

  /**
   * Loads all webonary entities related to a given language.
   */
  protected function loadWebonaryEntities() {
    $this->webonaryEntities = [];
    if (count($this->alphabetTerms) > 0) {
      try {
        // Loads the webonary entities related to the give language.
        $webonaryEntityStorage = $this->entityTypeManager->getStorage('webonary_entity');
        $results = $webonaryEntityStorage
          ->getQuery()
          ->accessCheck(TRUE)
          ->condition('field_alphabet_group.entity:taxonomy_term.tid', $this->alphabetTerms, 'IN')
          ->execute();
        $loadedEntities = $webonaryEntityStorage->loadMultiple($results);
        foreach ($loadedEntities as $loadedEntity) {
          $flexId = $loadedEntity->field_flexid->first()->getValue()['value'];
          $this->webonaryEntities[$flexId] = $loadedEntity;
        }
      }
      catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
        $this->importSchema->setError($this->t('Failed to load the webonary entities.'));
        $this->logger->error('Failed to load the webonary entities: ' . $e->getMessage());
      }
    }
  }

  /**
   * Parse the vernacular dictionary and import it to the database.
   *
   * @param string $language
   *   The tid of the language to filter the entries to be loaded.
   *
   * @return bool
   *   Whether the import was finished successfully.
   */
  protected function importVernacularDictionary(string $language): bool {
    try {
      $domDoc = $this->loadedDictionary;
      $xPath = new \DOMXPath($domDoc);
      $body = $xPath->query("/html/body/div");

      // Gets all valid entry flex-ids currently in use.
      $entryIds = $this->getValidFlexIds($body);

      // Get the media folder path.
      $mediaDestination = empty($this->mediaDestination)
        ? $this->webonaryService->getMediaFolder($this->importSchema->dictionary)
        : $this->mediaDestination;
      $mediaPath = $this->fileUrlGenerator->generateString($mediaDestination);

      $alphabet = '';
      foreach ($body as $child) {
        foreach ($child->attributes as $attr) {
          // Parse the dictionary alphabet.
          if ($attr->name == 'class' && $attr->value == 'letHead') {
            $lhPath = $xPath->query("./span", $child);
            $letterHead = $lhPath->item(0);
            $alphabet = trim($letterHead->nodeValue);
            if (str_contains($alphabet, ' ')) {
              $alphabet = explode(' ', $alphabet)[0];
            }
          }

          // Parse the entity.
          elseif ($attr->name == 'class' && $attr->value == 'entry') {
            // Parse the word.
            $wordPath = $xPath->query("./span[@class='mainheadword']/span/a", $child);
            if (empty($wordPath->item(0)->nodeValue)) {
              $wordPath = $xPath->query("./span[@class='mainheadword']/span/span/a", $child);
            }
            $word = $wordPath->item(0)->nodeValue;

            // Parse the grammars.
            $grammars = [];
            $queryPOS = "./span[@class='senses']/span[@class='sharedgrammaticalinfo']/span[@class='morphosyntaxanalysis']/span[@class='partofspeech']/span";
            $posChildren = $xPath->query($queryPOS, $child);
            foreach ($posChildren as $posChild) {
              $grammars[] = trim($posChild->nodeValue);
            }
            $queryPOS = "./span[@class='senses']/span[@class='sensecontent']/span[@class='sense']/span[@class='morphosyntaxanalysis']/span[@class='partofspeech']/span";
            $posChildren = $xPath->query($queryPOS, $child);
            foreach ($posChildren as $posChild) {
              $grammars[] = trim($posChild->nodeValue);
            }
            $grammars = array_unique($grammars);

            // Get the variants.
            $variants = [];
            $queryPOS = "./span[@class='variantformentrybackrefs']/span[@class='variantformentrybackref']/span/span/a";
            $posChildren = $xPath->query($queryPOS, $child);
            foreach ($posChildren as $posChild) {
              $variants[] = [
                'flexid' => str_replace('#', '', $posChild->getAttribute('href')),
                'word' => $posChild->nodeValue,
              ];
            }

            // Update the audio file paths.
            $queryPOS = "./span[@class='pronunciations']/span[@class='pronunciation']/span[@class='mediafiles']/span[@class='mediafile']/audio/source";
            $posChildren = $xPath->query($queryPOS, $child);

            foreach ($posChildren as $posChild) {
              $fileName = str_replace('\\', '/', $posChild->getAttribute('src'));
              $filePath = $mediaPath . '/' . $fileName;
              $posChild->setAttribute('src', $filePath);
            }

            // Update the images file paths.
            $queryPOS = "./span[@class='pictures']/div[@class='picture']/img";
            $posChildren = $xPath->query($queryPOS, $child);
            foreach ($posChildren as $posChild) {
              $fileName = str_replace('\\', '/', $posChild->getAttribute('src'));
              $filePath = $mediaPath . '/' . $fileName;
              $posChild->setAttribute('src', $filePath);
            }

            // Change anchors to links.
            $queryPOS = ".//a[not(@class)]";
            $posChildren = $xPath->query($queryPOS, $child);
            foreach ($posChildren as $posChild) {
              $refFlexId = str_replace('#', '', $posChild->getAttribute('href'));
              if (in_array($refFlexId, $entryIds)) {
                $posChild->setAttribute('href', '/webonary/entry/' . $refFlexId);
              }
              else {
                $posChild->removeAttribute('href');

                // Replace the <a> tag with an <span> tag.
                $newElement = $domDoc->createElement('span');
                $newElement->setAttribute('class', 'style-like-link flexid--' . $refFlexId);
                foreach ($posChild->attributes as $attribute) {
                  $newElement->setAttribute($attribute->nodeName, $attribute->nodeValue);
                }
                while ($posChild->hasChildNodes()) {
                  $originalChildNode = $posChild->childNodes->item(0);
                  $newElement->appendChild($originalChildNode);
                }
                $posChild->parentNode->replaceChild($newElement, $posChild);
              }
            }

            $queryPOS = ".//a[@class='CmFile']";
            $posChildren = $xPath->query($queryPOS, $child);
            foreach ($posChildren as $posChild) {
              $audioId = $posChild->getAttribute('href');
              if (str_ends_with($audioId, '.mp4')) {
                $fileName = str_replace('\\', '/', $audioId);
                $filePath = $mediaPath . '/' . $fileName;
                $posChild->setAttribute('href', $filePath);
                $posChild->setAttribute('target', '_blank');
              }
              else {
                $posChild->setAttribute('data-audio-id', $audioId);
                $posChild->setAttribute('href', '#');
              }
            }

            // Gets the child outer HTML as the word definition.
            $definition = $domDoc->saveHTML($child);
            $flexId = $child->getAttribute('id');

            $this->saveWebonaryEntry($flexId, $word, $definition, $language, $alphabet, $grammars, $variants);
          }
        }
      }

      return TRUE;
    }
    catch (\Exception $e) {
      $this->importSchema->setError($this->t('There was an error importing the vernacular dictionary.'));
      $this->logger->error('There was an error importing the vernacular dictionary: ' . $e->getMessage());
    }

    return FALSE;
  }

  /**
   * Parse the reversal dictionary and import it to the database.
   *
   * @param string $language
   *   The tid of the language to filter the entries to be loaded.
   *
   * @return bool
   *   Whether the import was finished successfully.
   */
  protected function importReversalDictionary(string $language): bool {
    try {
      $domDoc = $this->loadedDictionary;
      $xPath = new \DOMXPath($domDoc);
      $body = $xPath->query("/html/body/div");

      // Gets all valid entry flex-ids currently in use.
      $entryIds = $this->getValidFlexIds($body);

      $alphabet = '';
      foreach ($body as $child) {
        foreach ($child->attributes as $attr) {
          // Parse the dictionary alphabet.
          if ($attr->name == 'class' && $attr->value == 'letHead') {
            $lhPath = $xPath->query("./span", $child);
            $letterHead = $lhPath->item(0);
            $alphabet = trim($letterHead->nodeValue);
            if (str_contains($alphabet, ' ')) {
              $alphabet = explode(' ', $alphabet)[0];
            }
          }

          // Parse the entity.
          elseif ($attr->name == 'class' && $attr->value == 'reversalindexentry') {
            // Parse the word.
            $wordPath = $xPath->query("./span[@class='reversalform']/span", $child);
            $word = $wordPath->item(0)->nodeValue;

            // Change anchors to links.
            $queryPOS = ".//a[not(@class)]";
            $posChildren = $xPath->query($queryPOS, $child);
            foreach ($posChildren as $posChild) {
              $refFlexId = str_replace('#', '', $posChild->getAttribute('href'));
              if (in_array($refFlexId, $entryIds)) {
                $posChild->setAttribute('href', '/webonary/entry/' . $refFlexId);
              }
              else {
                $posChild->removeAttribute('href');

                // Replace the <a> tag with an <span> tag.
                $newElement = $domDoc->createElement('span');
                $newElement->setAttribute('class', 'style-like-link flexid--' . $refFlexId);
                foreach ($posChild->attributes as $attribute) {
                  $newElement->setAttribute($attribute->nodeName, $attribute->nodeValue);
                }
                while ($posChild->hasChildNodes()) {
                  $originalChildNode = $posChild->childNodes->item(0);
                  $newElement->appendChild($originalChildNode);
                }
                $posChild->parentNode->replaceChild($newElement, $posChild);
              }
            }

            // Gets the child outer HTML as the word definition.
            $definition = $domDoc->saveHTML($child);
            $flexId = $child->getAttribute('id');
            $grammars = [];
            $variants = [];
            $this->saveWebonaryEntry($flexId, $word, $definition, $language, $alphabet, $grammars, $variants);
          }
        }
      }

      return TRUE;
    }
    catch (\Exception $e) {
      $this->importSchema->setError($this->t('There was an error importing the reversal dictionary.'));
      $this->logger->error('There was an error importing the reversal dictionary: ' . $e->getMessage());
    }

    return FALSE;
  }

  /**
   * Save a webonary entity into the database.
   *
   * @param string $flexId
   *   The FLExId extracted from the parsed file.
   * @param string $word
   *   The word extracted from the parsed file.
   * @param string $definition
   *   The HTML code with the word definition extracted from the parsed file.
   * @param string $language
   *   The tid of the language being imported.
   * @param string $alphabet
   *   The alphabet letter extracted from the parsed file.
   * @param array $grammars
   *   The list of related grammars extracted from the parsed file.
   * @param array $variants
   *   The list of variants for the word.
   *
   * @return bool
   *   Whether the save was successful.
   */
  protected function saveWebonaryEntry(string $flexId, string $word, string $definition, string $language, string $alphabet, array $grammars, array $variants): bool {
    try {
      // Gets the alphabet tid.
      if (!array_key_exists($alphabet, $this->alphabetTerms)) {
        // Adds the term to the database.
        $termData = [
          'vid' => 'webonary_language_alphabet',
          'parent' => $language,
          'name' => $alphabet,
          'field_webonary_lang_code' => $alphabet,
          'field_webonary_display_filter' => 1,
          'field_webonary_display_keyboard' => 1,
          'weight' => count($this->alphabetTerms),
        ];
        $term = $this->entityTypeManager->getStorage('taxonomy_term')->create($termData);
        $term->save();
        $this->alphabetTerms[$alphabet] = $term->id();
      }
      $alphabetTerm = $this->alphabetTerms[$alphabet];

      // Gets the grammar tids.
      $grammarTerms = [];
      foreach ($grammars as $grammar) {
        if (!array_key_exists($grammar, $this->grammarTerms)) {
          // Adds the term to the database.
          $termData = [
            'vid' => 'webonary_grammar_category',
            'name' => $grammar,
            'field_webonary_category_abbr' => $grammar,
          ];
          $term = $this->entityTypeManager->getStorage('taxonomy_term')->create($termData);
          $term->save();
          $this->grammarTerms[$grammar] = $term->id();
        }
        $grammarTerms[] = $this->grammarTerms[$grammar];
      }

      // Checks if webonary content exists.
      if (array_key_exists($flexId, $this->webonaryEntities)) {
        // Updates the current entity.
        $webonaryEntity = $this->webonaryEntities[$flexId];
        $webonaryEntity->field_definition->value = $definition;
        $webonaryEntity->field_grammar_category = $grammarTerms;
        $webonaryEntity->field_variants = $variants;
        $webonaryEntity->field_alphabet_group = $alphabetTerm;
        $webonaryEntity->setTitle($word);
      }
      else {
        // Creates a new entity.
        $values = [
          'field_flexid' => $flexId,
          'field_definition' => [
            'value' => $definition,
            'format' => 'webonary_html',
          ],
          'field_grammar_category' => $grammarTerms,
          'field_variants' => $variants,
          'field_alphabet_group' => $alphabetTerm,
          'title' => $word,
          'bundle' => 'webonary',
          'field_weight' => count($this->webonaryEntities),
        ];
        $webonaryEntity = $this->entityTypeManager->getStorage('webonary_entity')->create($values);
        $this->webonaryEntities[$flexId] = $webonaryEntity;
      }

      $webonaryEntity->save();

      return TRUE;
    }
    catch (\Exception $e) {
      $this->importSchema->setError($this->t('There was an error creating the webonary entity.'));
      $this->logger->error('There was an error creating the webonary entity: ' . $e->getMessage());
    }

    return FALSE;
  }

  /**
   * Gets all valid entry flex-ids currently in use.
   *
   * @param object $body
   *   The DOMDocument object containing the body of the imported file.
   *
   * @throws \Exception
   */
  protected function getValidFlexIds($body): array {
    // Gets all valid entry flex-ids currently in database.
    $entryIds = $this->connection
      ->select('webonary_entity', 't')
      ->fields('t', ['field_flexid'])
      ->distinct(TRUE)
      ->condition('bundle', 'webonary')
      ->condition('status', 1)
      ->execute()
      ->fetchCol(0);

    // Gets all valid entry flex-ids from the imported file.
    foreach ($body as $child) {
      if (($child->getAttribute('class') === 'entry') && (!empty($child->getAttribute('id')))) {
        $entryIds[] = $child->getAttribute('id');
      }
    }

    return $entryIds;
  }

}
