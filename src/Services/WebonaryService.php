<?php

namespace Drupal\webonary\Services;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\State\State;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\webonary\Classes\EImportDictionary;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * This service will execute all import actions.
 */
class WebonaryService {

  use StringTranslationTrait;

  /**
   * Entity type manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Entity type manager interface.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected ConfigFactory $config;

  /**
   * The current route.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected CurrentRouteMatch $currentRoute;

  /**
   * The current HTTP request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected Request $currentRequest;

  /**
   * The state manager.
   *
   * @var \Drupal\Core\State\State
   */
  protected State $state;

  /**
   * Constructs an ImportService object.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The service for managing entities.
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   The service for managing configurations.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The current route.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The current request stack.
   * @param \Drupal\Core\State\State $state
   *   The state manager.
   */
  public function __construct(TranslationInterface $string_translation, EntityTypeManagerInterface $entityTypeManager, ConfigFactory $config, CurrentRouteMatch $current_route_match, RequestStack $request_stack, State $state) {
    $this->stringTranslation = $string_translation;
    $this->entityTypeManager = $entityTypeManager;
    $this->config = $config;
    $this->currentRoute = $current_route_match;
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->state = $state;
  }

  /**
   * Get the dictionaries' information.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getDictionaries(): array {
    $dictionaries = [
      EImportDictionary::VERNACULAR => NULL,
      EImportDictionary::REVERSAL => NULL,
    ];

    $config = $this->config->getEditable('webonary.settings');
    $vernacularLanguage = $config->get('vernacular_lang');
    $reversalLanguage = $config->get('reversal_lang');

    $langTerms = $this->entityTypeManager
      ->getStorage('taxonomy_term')
      ->loadTree('webonary_language_alphabet', 0, 1);

    foreach ($langTerms as $langTerm) {
      switch ($langTerm->tid) {
        case $vernacularLanguage:
          $key = EImportDictionary::VERNACULAR;
          break;

        case $reversalLanguage:
          $key = EImportDictionary::REVERSAL;
          break;

        default:
          $key = NULL;
          break;
      }

      if (isset($key)) {
        $dictionaries[$key] = [
          'id' => $langTerm->tid,
          'name' => $langTerm->name,
        ];
      }
    }

    return $dictionaries;
  }

  /**
   * Get the dictionary type based on the taxonomy id.
   *
   * @param string $tid
   *   The taxonomy id.
   *
   * @return string
   *   The dictionary type.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getDictionaryType(string $tid): string {
    $dictionaries = $this->getDictionaries();
    foreach ($dictionaries as $key => $dictionary) {
      if ($dictionary['id'] === $tid) {
        return $key;
      }
    }

    return '';
  }

  /**
   * Get the alphabet information from the current URL.
   *
   * @return array
   *   Information about the alphabet.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getAlphabetFromUrl(): array {
    // Gets the configuration settings.
    $config = $this->config->getEditable('webonary.settings');
    $vernacular_path = $config->get('vernacular_path') ?? 'vernacular';
    $reversal_path = $config->get('reversal_path') ?? 'reversal';

    // Validates the dictionary path.
    $dictionary = $this->currentRoute->getParameter('dictionary');
    switch ($dictionary) {
      case $vernacular_path:
        $key = EImportDictionary::VERNACULAR;
        $parent = $config->get('vernacular_lang') ?? '';
        break;

      case $reversal_path:
        $key = EImportDictionary::REVERSAL;
        $parent = $config->get('reversal_lang') ?? '';
        break;

      default:
        $parent = '';
        $key = '';
        break;
    }

    if (empty($parent) || empty($key)) {
      return [];
    }

    // Gets all alphabet related to the dictionary language.
    $tids = $this->entityTypeManager
      ->getStorage('taxonomy_term')
      ->getQuery()
      ->condition('parent', $parent)
      ->accessCheck(TRUE)
      ->execute();

    return [
      'dictionary' => $key,
      'language' => $parent,
      'ids' => $tids,
      'route' => $this->currentRoute->getRouteName(),
      'path' => $dictionary,
      'filter' => $this->currentRequest->get('alphabet') ?? '',
    ];
  }

  /**
   * Gets all CSS styles currently in use by the system.
   *
   * @return array
   *   The style URI for each dictionary.
   */
  public function getStyleFiles(): array {
    return [
      EImportDictionary::VERNACULAR => $this->state->get('webonary.style.vernacular') ?? '',
      EImportDictionary::REVERSAL => $this->state->get('webonary.style.reversal') ?? '',
    ];
  }

  /**
   * Set a style file to a dictionary.
   *
   * @param string $dictionary
   *   The dictionary to set the style file to.
   * @param string $value
   *   The value to store for the dictionary.
   */
  public function setStyleFile(string $dictionary, string $value) {
    $this->state->set('webonary.style.' . $dictionary, $value);
  }

  /**
   * Delete a style file to a dictionary.
   *
   * @param string $dictionary
   *   The dictionary to delete the style file reference.
   */
  public function deleteStyleFile(string $dictionary) {
    $this->state->delete('webonary.style.' . $dictionary);
  }

  /**
   * Gets all media folders currently in use by the system.
   *
   * @return array
   *   The folder path for each dictionary.
   */
  public function getMediaFolders(): array {
    return [
      EImportDictionary::VERNACULAR => $this->state->get('webonary.media.vernacular') ?? '',
      EImportDictionary::REVERSAL => $this->state->get('webonary.media.reversal') ?? '',
    ];
  }

  /**
   * Gets the currently media folder in use for a specific dictionary.
   *
   * @param string $dictionary
   *   The dictionary to get the media folder for.
   *
   * @return string
   *   The folder path for the dictionary.
   */
  public function getMediaFolder(string $dictionary): string {
    return $this->getMediaFolders()[$dictionary];
  }

  /**
   * Set a media folder to a dictionary.
   *
   * @param string $dictionary
   *   The dictionary to set the media folder to.
   * @param string $value
   *   The value to store for the dictionary.
   */
  public function setMediaFolder(string $dictionary, string $value) {
    $this->state->set('webonary.media.' . $dictionary, $value);
  }

  /**
   * Delete a media folder to a dictionary.
   *
   * @param string $dictionary
   *   The dictionary to delete the media folder reference.
   */
  public function deleteMediaFolder(string $dictionary) {
    $this->state->delete('webonary.media.' . $dictionary);
  }

  /**
   * Get a webonary entity based on a FLExId.
   *
   * @param string $flexid
   *   The FLExId of the entity to be returned.
   *
   * @return array
   *   The entity and the variant.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getWebonaryEntity(string $flexid): array {
    $data = [
      'entity' => NULL,
      'variant' => NULL,
    ];

    if (!empty($flexid)) {
      $storage = $this->entityTypeManager->getStorage('webonary_entity');
      $wid = $storage->getQuery()
        ->accessCheck(FALSE)
        ->condition('status', 1)
        ->condition('field_flexid', $flexid)
        ->execute();

      if (empty($wid)) {
        $isVariant = TRUE;
        $wid = $storage->getQuery()
          ->condition('field_variants.flexid', $flexid)
          ->accessCheck(FALSE)
          ->execute();
      }
      else {
        $isVariant = FALSE;
      }

      // Loads the entity.
      if (!empty($wid)) {
        $wid = reset($wid);
        $entity = $storage->load($wid);
        if (isset($entity)) {
          $data['entity'] = $entity;

          if ($isVariant) {
            foreach ($entity->field_variants as $variant) {
              if ($variant->flexid === $flexid) {
                $data['variant'] = $variant->word;
              }
            }
          }
        }
      }
    }

    return $data;
  }

  /**
   * Get the alphabet for the vernacular language.
   *
   * @return array
   *   The alphabet for the vernacular language.
   */
  public function buildVernacularKeyboard(): array {
    $taxonomies = $this->entityTypeManager->getStorage('taxonomy_term');
    $vernacular = $this->config->get('webonary.settings')->get('vernacular_lang');
    $alphabetTerms = $taxonomies->loadTree('webonary_language_alphabet', $vernacular, 1);
    $tids = array_map(fn($alphabetTerm) => $alphabetTerm->tid, $alphabetTerms);
    $alphabetTerms = $taxonomies->loadMultiple($tids);

    $items = [];
    foreach ($alphabetTerms as $alphabetTerm) {
      if ($alphabetTerm->field_webonary_display_keyboard->value == 1 && $alphabetTerm->status->value == 1) {
        $char = $alphabetTerm->label();
        $items[] = Markup::create(
          '<span data-key="' . $char . '">' . $char . '</span>'
        );
      }
    }

    return [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'webonary-keyboard',
        'style' => 'display: none',
      ],
      'list' => [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#items' => $items,
      ],
    ];
  }

  /**
   * Render the vernacular keyboard.
   *
   * @return array
   *   The rendered vernacular keyboard.
   */
  public function renderVernacularKeyboard(): array {
    return [
      '#theme' => 'webonary_search_bar',
      '#keyboard' => $this->buildVernacularKeyboard(),
      '#attached' => [
        'library' => [
          'webonary/search',
        ],
      ],
    ];
  }

}
