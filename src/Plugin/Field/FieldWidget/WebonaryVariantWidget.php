<?php

namespace Drupal\webonary\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'field_webonary_variant' widget.
 *
 * @FieldWidget(
 *   id = "field_webonary_variant_widget",
 *   label = @Translation("Webonary Variant Default"),
 *   field_types = {
 *     "field_webonary_variant"
 *   },
 * )
 */
class WebonaryVariantWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {

    $element['word'] = [
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->word ?? NULL,
      '#required' => FALSE,
      '#maxlength' => 255,
      '#size' => 15,
    ];

    $element['flexid'] = [
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->flexid ?? NULL,
      '#required' => FALSE,
      '#maxlength' => 255,
      '#size' => 30,
    ];

    // Wrap the field in a detail element.
    $element += [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['container-inline'],
      ],
    ];

    return $element;
  }

}
