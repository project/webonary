<?php

namespace Drupal\webonary\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'field_webonary_variant' formatter.
 *
 * @FieldFormatter(
 *   id = "field_webonary_variant_formatter",
 *   label = @Translation("Webonary Variant Default"),
 *   field_types = {
 *     "field_webonary_variant",
 *   },
 * )
 */
class WebonaryVariantFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode = NULL): array {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'markup',
        '#markup' => $item->word,
      ];
    }

    return $elements;
  }

}
