<?php

namespace Drupal\webonary\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'field_webonary_variant' field type.
 *
 * @FieldType(
 *   id = "field_webonary_variant",
 *   label = @Translation("Webonary Variant"),
 *   category = "General",
 *   default_widget = "field_webonary_variant_widget",
 *   default_formatter = "field_webonary_variant_formatter"
 * )
 */
class WebonaryVariantItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    return [
      'columns' => [
        'flexid' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ],
        'word' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    $word = $this->get('word')->getValue() ?? '';
    $flexid = $this->get('flexid')->getValue() ?? '';
    return $flexid === '' && $word === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {

    $properties['flexid'] = DataDefinition::create('string')
      ->setLabel(t('FLExId'))
      ->setRequired(TRUE);

    $properties['word'] = DataDefinition::create('string')
      ->setLabel(t('Word'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints(): array {
    $constraints = parent::getConstraints();

    $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();

    $constraints[] = $constraint_manager->create('ComplexData', [
      'flexid' => [],
      'word' => [],
    ]);

    return $constraints;
  }

}
