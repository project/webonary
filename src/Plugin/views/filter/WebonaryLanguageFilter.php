<?php

namespace Drupal\webonary\Plugin\views\filter;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filters data related to webonary language.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("webonary_lang_views_filter")
 */
class WebonaryLanguageFilter extends FilterPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The webonary languages.
   *
   * @var array
   */
  protected array $languages;

  /**
   * The alphabet list per language.
   *
   * @var array
   */
  protected array $alphabet;

  /**
   * Constructs a Handler object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->languages = [];
    $this->alphabet = [];

    try {
      $lang_terms = $entity_type_manager
        ->getStorage('taxonomy_term')
        ->loadTree('webonary_language_alphabet');

      foreach ($lang_terms as $lang_term) {
        $parent = $lang_term->parents[0] ?? NULL;
        if (isset($parent)) {
          if ($parent == 0) {
            $this->languages[$lang_term->tid] = $lang_term->name;
            $this->alphabet[$lang_term->tid] = [];
          }
          else {
            $this->alphabet[$parent][$lang_term->tid] = $lang_term->tid;
          }
        }
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      $this->messenger()->addError('Failed to load webonary languages.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildExposeForm(&$form, FormStateInterface $form_state): void {
    parent::buildExposeForm($form, $form_state);

    unset($form['expose']['required']);
    unset($form['expose']['multiple']);
    unset($form['expose']['remember']);
    unset($form['expose']['operator_id']);
    unset($form['expose']['remember_roles']);
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state): void {
    parent::valueForm($form, $form_state);

    $form['value'] = [
      '#type' => 'select',
      '#title' => $this->t('Language'),
      '#options' => $this->languages,
      '#required' => FALSE,
      '#default_value' => array_key_first($this->languages),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query(): void {
    $value = $this->value;
    if (is_array($value)) {
      $value = reset($value);
    }

    if (!empty($value) && array_key_exists($value, $this->alphabet)) {
      $alphabet = $this->alphabet[$value];
      if (empty($alphabet)) {
        $alphabet = [-1];
      }

      $group = $this->options['group'];
      $field = 'webonary_entity.field_alphabet_group IN (:webonary_entity_field_alphabet_group[])';
      $value = [
        ':webonary_entity_field_alphabet_group[]' => $alphabet,
      ];
      $operator = 'formula';

      $this->query->addWhere($group, $field, $value, $operator);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary(): TranslatableMarkup {
    if (!empty($this->options['exposed'])) {
      return $this->t('exposed');
    }
    elseif ($this->options['value'] === 1) {
      return $this->t('Yes');
    }
    else {
      return $this->t('No');
    }
  }

}
