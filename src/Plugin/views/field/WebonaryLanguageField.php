<?php

namespace Drupal\webonary\Plugin\views\field;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field handler to display the language related to the webonary entity.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("webonary_lang_field")
 */
class WebonaryLanguageField extends FieldPluginBase {

  /**
   * The webonary languages.
   *
   * @var array
   */
  protected array $languages;

  /**
   * The alphabet list per language.
   *
   * @var array
   */
  protected array $alphabet;

  /**
   * Constructs a Handler object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->languages = [];
    $this->alphabet = [];

    try {
      $lang_terms = $entity_type_manager
        ->getStorage('taxonomy_term')
        ->loadTree('webonary_language_alphabet');

      foreach ($lang_terms as $lang_term) {
        $parent = $lang_term->parents[0] ?? NULL;
        if (isset($parent)) {
          if ($parent == 0) {
            $this->languages[$lang_term->tid] = $lang_term->name;
          }
          else {
            $this->alphabet[$lang_term->tid] = $parent;
          }
        }
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      $this->messenger()->addError('Failed to load webonary languages.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values): array {
    if (isset($values->taxonomy_term_field_data_webonary_entity_tid) &&
        isset($this->alphabet[$values->taxonomy_term_field_data_webonary_entity_tid]) &&
        isset($this->languages[$this->alphabet[$values->taxonomy_term_field_data_webonary_entity_tid]])
    ) {
      return [
        '#markup' => $this->languages[$this->alphabet[$values->taxonomy_term_field_data_webonary_entity_tid]],
      ];
    }

    return [];
  }

}
