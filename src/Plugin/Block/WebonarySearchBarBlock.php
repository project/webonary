<?php

namespace Drupal\webonary\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\webonary\Services\WebonaryService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Webonary search bar block.
 *
 * @Block(
 *   id = "webonary_search_bar_block",
 *   admin_label = @Translation("Webonary: Search Bar"),
 * )
 */
class WebonarySearchBarBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The webonary service.
   *
   * @var \Drupal\webonary\Services\WebonaryService
   */
  protected WebonaryService $webonaryService;

  /**
   * Creates a LocalActionsBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\webonary\Services\WebonaryService $webonary_service
   *   A webonary service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, WebonaryService $webonary_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->webonaryService = $webonary_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('webonary')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return $this->webonaryService->renderVernacularKeyboard();
  }

}
