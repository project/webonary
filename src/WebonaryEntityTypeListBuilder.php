<?php

namespace Drupal\webonary;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of webonary entity type entities.
 *
 * @see \Drupal\webonary\Entity\WebonaryEntityType
 */
class WebonaryEntityTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['title'] = $this->t('Label');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row['title'] = [
      'data' => $entity->label(),
      'class' => ['menu-label'],
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No webonary entity types available. <a href=":link">Add webonary entity type</a>.',
      [':link' => Url::fromRoute('entity.webonary_entity_type.add_form')->toString()]
    );

    return $build;
  }

}
