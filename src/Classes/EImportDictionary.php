<?php

namespace Drupal\webonary\Classes;

/**
 * Enum for the dictionary to import.
 */
abstract class EImportDictionary {

  const VERNACULAR = 'vernacular';

  const REVERSAL = 'reversal';

}
