<?php

namespace Drupal\webonary\Classes;

/**
 * Enum for the import type.
 */
abstract class EImportType {

  const FULL = 'full';

  const PARTIAL = 'partial';

}
