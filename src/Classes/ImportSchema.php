<?php

namespace Drupal\webonary\Classes;

/**
 * Wraps the information about the import.
 */
class ImportSchema {

  /**
   * The public folder for saving the style files.
   */
  const STYLE_FOLDER = 'public://webonary/css';

  /**
   * The public folder for saving the media files.
   */
  const MEDIA_FOLDER = 'public://webonary/media';

  /**
   * The dictionary to import.
   *
   * Use EImportDictionary to get the acceptable values.
   *
   * @var string
   */
  public string $dictionary;

  /**
   * The type of import.
   *
   * Use EImportType to get the acceptable values.
   *
   * @var string
   */
  public string $type;

  /**
   * The path to the XHTML file to be imported.
   *
   * @var string
   */
  public string $dataFilePath;

  /**
   * The path to the CSS file to be imported.
   *
   * @var string
   */
  public string $styleFilePath;

  /**
   * The path to the ZIP file containing all media to be imported.
   *
   * @var string
   */
  public string $mediaFilePath;

  /**
   * Whether the import was finished successfully or not.
   *
   * @var bool
   */
  public bool $imported;

  /**
   * Any error message thrown during the import.
   *
   * @var array
   */
  public array $errorMessages;

  /**
   * Creates a new object containing information about the data to import.
   *
   * @param string $dictionary
   *   The dictionary to import. Use EImportDictionary.
   * @param string $type
   *   The type of import. EImportType.
   * @param string $data_file_path
   *   The path to the XHTML file to be imported.
   * @param string $style_file_path
   *   The path to the CSS file to be imported.
   * @param string $media_file_path
   *   The path to the ZIP file containing all media to be imported.
   */
  public function __construct(string $dictionary, string $type, string $data_file_path, string $style_file_path = '', string $media_file_path = '') {
    $this->dictionary = $dictionary;
    $this->type = $type;
    $this->dataFilePath = $data_file_path;
    $this->styleFilePath = $style_file_path;
    $this->mediaFilePath = $media_file_path;

    $this->imported = FALSE;
    $this->errorMessages = [];
  }

  /**
   * Sets a new error message.
   *
   * @param string $errorMessage
   *   The error message to be set.
   */
  public function setError(string $errorMessage): void {
    $this->errorMessages[] = $errorMessage;
  }

}
