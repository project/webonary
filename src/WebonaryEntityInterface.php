<?php

namespace Drupal\webonary;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a webonary entity type.
 */
interface WebonaryEntityInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the webonary entity title.
   *
   * @return string
   *   Title of the webonary entity.
   */
  public function getTitle(): string;

  /**
   * Sets the webonary entity title.
   *
   * @param string $title
   *   The webonary entity title.
   *
   * @return \Drupal\webonary\WebonaryEntityInterface
   *   The called webonary entity.
   */
  public function setTitle(string $title): WebonaryEntityInterface;

  /**
   * Gets the webonary entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the webonary entity.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the webonary entity creation timestamp.
   *
   * @param int $timestamp
   *   The webonary entity creation timestamp.
   *
   * @return \Drupal\webonary\WebonaryEntityInterface
   *   The called webonary entity.
   */
  public function setCreatedTime(int $timestamp): WebonaryEntityInterface;

  /**
   * Returns the webonary entity status.
   *
   * @return bool
   *   TRUE if the webonary entity is enabled, FALSE otherwise.
   */
  public function isEnabled(): bool;

  /**
   * Sets the webonary entity status.
   *
   * @param bool $status
   *   TRUE to enable this webonary entity, FALSE to disable.
   *
   * @return \Drupal\webonary\WebonaryEntityInterface
   *   The called webonary entity.
   */
  public function setStatus(bool $status): WebonaryEntityInterface;

}
