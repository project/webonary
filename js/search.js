/**
 * @file
 * Script for including a virtual keyboard for the vernacular language.
 */

(function ($, Drupal) {

  Drupal.behaviors.search = {
    attach: function (context, settings) {
      once('search', 'html', context).forEach((html) => {
        renderVernacularKeyboard($, $(html));
        highlightKeywordsInText($, $(html));
      });
    },
  };

})(jQuery, Drupal);

/**
 * Render the Vernacular keyboard.
 *
 * @param $
 *   The jQuery object.
 * @param $html
 *   The HTML element.
 */
function renderVernacularKeyboard($, $html) {
  let $input = $html.find('#edit-word');
  if ($input.length === 0) {
    $input = $html.find('#edit-webonary--search-word');
  }

  const $keyboard = $html.find('#webonary-keyboard');
  $keyboard.detach().appendTo('.form-item-word');
  $keyboard.css({ display: 'block' });

  $keyboard.find('ul li span').each((_, item) => {
    const $item = $(item);
    $item.click(() => {
      const character = $item.data('key');
      const inputField = $input[0];
      let inputValue = $input.val();

      // Remove the selected text.
      if (inputField.selectionEnd > inputField.selectionStart) {
        inputValue = inputValue.slice(0, inputField.selectionStart) + inputValue.slice(inputField.selectionEnd);
      }

      // Get the cursor position.
      const cursorPos = getCursorPosition(inputField);
      if (cursorPos >= inputValue.length) {

        // Append character to the end of the text.
        $input.val(inputValue + character);
      }
      else {
        // Split the current text.
        const textBefore = inputValue.substring(0, cursorPos);
        const textAfter = inputValue.substring(cursorPos);

        // Set the new text.
        $input.val(textBefore + character + textAfter);

        // Set the cursor position.
        setCursorPosition(inputField, cursorPos + character.length);
      }

      $input.focus();
    });
  });
}

/**
 * Get the cursor position in the input field.
 *
 * @param inputField
 *   The input field.
 * @returns {number|number|*}
 *   The cursor position.
 */
function getCursorPosition(inputField) {
  if (document.selection) {
    inputField.focus();
    let theSelectionRange = document.selection.createRange();
    theSelectionRange.moveStart('character', -inputField.value.length);
    return theSelectionRange.text.length;
  }

  if (inputField.selectionStart || inputField.selectionStart === 0) {
    return inputField.selectionStart;
  }

  return inputField.value.length;
}

/**
 * Set the cursor position in the input field.
 *
 * @param inputField
 *   The input field.
 * @param position
 *   The cursor position.
 */
function setCursorPosition(inputField, position) {
  if (inputField.setSelectionRange) {
    inputField.setSelectionRange(position, position);
  }
  else if (inputField.createTextRange) {
    const range = inputField.createTextRange();
    range.collapse(true);
    range.moveEnd('character', position);
    range.moveStart('character', position);
    range.select();
  }
}

/**
 * Highlight the keywords inside the returned text.
 *
 * @param $
 *   The jQuery object.
 * @param $html
 *   The HTML element.
 */
function highlightKeywordsInText($, $html) {
  const $input = $html.find('#edit-word');
  const $content = $html.find('.view-id-webonary.view-display-id-webonary_search .view-content');
  if ($content.length === 0) {
    return;
  }

  // Get the keywords.
  let keywords = $input.val();
  if (!keywords) {
    return;
  }
  keywords = keywords.split(' ');

  // Include highlight placeholders for each entry.
  $content.find('.views-row').each((_, item) => {
    const $item = $(item);

    // Get only the text nodes.
    const $textNodes = $item.find(':not(iframe)').addBack().contents().filter(function () {
      return this.nodeType === 3;
    });

    $textNodes.each((_, element) => {
      let text = element.nodeValue;

      // Include a placeholder for each keyword.
      keywords.forEach((keyword) => {
        const regex = new RegExp(keyword.toUpperCase(), 'gi');

        // Get the positions of the keyword in the text.
        const positions = [];
        while (match = regex.exec(text)) {
          positions.push(match.index);
        }

        // Include placeholders before and after each keyword.
        for (let i = positions.length; i > 0; i--) {
          const startPosition = positions[i - 1];
          const endPosition = startPosition + keyword.length;

          text = text.slice(0, endPosition) + '#mark-close#' + text.slice(endPosition);
          text = text.slice(0, startPosition) + '#mark-open#' + text.slice(startPosition);
        }
      });

      element.nodeValue = text;
    });

  });

  // Highlight the keywords by replacing the placeholders.
  let text = $content.html();
  text = text.replace(new RegExp('#mark-open#', 'gi'), '<mark>');
  text = text.replace(new RegExp('#mark-close#', 'gi'), '</mark>');
  $content.html(text);
}
