/**
 * @file
 * Script for including a virtual keyboard for the vernacular language.
 */

(function ($, Drupal) {

  Drupal.behaviors.webonary_audio = {
    attach: function (context, settings) {
      once('webonary_audio', 'html', context).forEach(function (html) {
        $(html).find('.CmFile').each(function (index) {
          const audio_id = $(this).data('audio-id');
          if (audio_id) {
            $(this).click(function (e) {
              e.preventDefault();
              $(audio_id)[0].play();
            });
          }
        });
      });
    }
  };

})(jQuery, Drupal);
