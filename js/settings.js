/**
 * @file
 * Script for the settings page.
 */

(function ($, Drupal) {

  Drupal.behaviors.settings = {
    attach: function (context, settings) {
      once('settings', 'html', context).forEach(function (html) {

        const $vernacularLang = $(html).find('#edit-vernacular-lang');
        const $reversalLang = $(html).find('#edit-reversal-lang');

        // Disable the possibility of selecting the same language twice.
        const disableOptions = () => {
          $vernacularLang.find('option').prop('disabled', false);
          $reversalLang.find('option').prop('disabled', false);

          // Disable from reversal the selected vernacular option.
          const vernacularValue = $vernacularLang.val();
          if (vernacularValue) {
            $reversalLang
              .find(`option[value="${vernacularValue}"]`)
              .prop('disabled', true);
          }

          // Disable from vernacular the selected reversal option.
          const reversalValue = $reversalLang.val();
          if (reversalValue) {
            $vernacularLang
              .find(`option[value="${reversalValue}"]`)
              .prop('disabled', true);
          }
        };

        // Validate the options on change.
        $vernacularLang.change(disableOptions);
        $reversalLang.change(disableOptions);

        // Validate the options on load.
        disableOptions();

      });
    }
  };

})(jQuery, Drupal);
